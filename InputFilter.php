<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Consulting247;
use Consulting247\Message;
/**
 * Description of InputChecker
 *
 * @author ken
 */
namespace Consulting247;
use libphonenumber\PhoneNumberUtil;


//rename InputFilter ASAP or sooner
class InputFilter{
    private $msg;
    private $data;
    private $inputType;
    private $countryCode;
    private $inputNonBlankValueRequired;
    
    function __construct($inputType = INPUT_POST,$countryCode='US',$inputNonBlankValueRequired=false) {
        $this->msg = new Message();
        $this->inputType = $inputType;
        $this->countryCode = $countryCode;
        $this->inputNonBlankValueRequired = $inputNonBlankValueRequired;
    }

    function sanitize($index,$stub=null){        
        $this->data[$index] = filter_input($this->inputType, $index,FILTER_SANITIZE_STRING);
        
        if (($this->inputNonBlankValueRequired) && 
            (strlen(trim($this->data[$index]))<=0)){
            $this->msg->addMessage($index, ucfirst(trim($stub." may not be blank")));
        }
        return $this;
    }

    function sanitizeArray($index,$stub=null){        
        $this->data[$index] = filter_input($this->inputType, $index,FILTER_SANITIZE_STRING,FILTER_REQUIRE_ARRAY);
        foreach ($this->data AS $key=>$value){
            if (($this->inputNonBlankValueRequired) && 
                (strlen(trim($value))<=0)){
                $this->msg->addMessage($index, ucfirst(trim($stub." may not be blank")));
            }
        }
        return $this;
    }
    
    function sanitizeEmail($index){
        //not working quite right yet what happens when email is not a required feild;
        $email = filter_input($this->inputType, $index,FILTER_SANITIZE_EMAIL);
       
        if ((!filter_var($email,FILTER_VALIDATE_EMAIL))){
            $this->addMessage($index, "Is not a valid email address");
        }       
        $this->data[$index]=$email;
        
        return $this;
    }
    
    function sanitizeURL($index){
        $url = filter_input($this->inputType, $index,FILTER_SANITIZE_URL);
        if (is_null($url)){
            $this->addMessage($index, "URL may not be blank");
            $this->data[$index]=$url;
            return $this;
        }
       
        //make sure that the component works
        if ((substr($url,0,7) !=="http://") &&
            (substr($url,0,8) !=="https://")){
            $url = "http://".$url;       
        }

        //is in Proper form
        if (filter_var($url, FILTER_VALIDATE_URL)){
            //see if host is valid
            $urlComponents= parse_url($url);
            
            $i1 = (key_exists("host", $urlComponents))?"host":"path";
            $urlPathArray = explode('/', $urlComponents[$i1]);
       
            //is the host good
            $this->urlIPAddress = filter_var(gethostbyname($urlPathArray[0]),FILTER_VALIDATE_IP);
            if (filter_var($this->urlIPAddress,FILTER_VALIDATE_IP) &&
                ($this->urlIPAddress !=="92.242.140.21")){
                //92.242.140.21 is a redirection service 
                $headers= get_headers($url);
                 if($headers[0] === 'HTTP/1.1 404 Not Found') {
                    //var_dump($headers);
                    $this->urlHeaders = $headers;
                    $this->addMessage($index, "Page Not Found");            
                }
            }
            else{
                 $this->addMessage($index, "URL Host can not locate");
            }
        }
        else{
            $this->addMessage($index, "URL is not set valid");
        }
        $this->data[$index]=$url;
        
        return $this;
    }
    
    function sanitizeInt($index){
        //not working quite right yet what happens when email is not a required feild;
        $myInt = filter_input($this->inputType, $index,FILTER_SANITIZE_NUMBER_INT);
             
        if (($this->inputNonBlankValueRequired) && 
            (strlen(trim($myInt))<=0)){
            $this->sanitize($index);
            $this->msg->addMessage($index, ucfirst("'{$this->data[$index]}' is not valid"));
        }
        else{
            $this->data[$index]=  intval($myInt);
        }
        return $this;
    }
    
    function sanitizeIntArray($index){
        $myIntArray = filter_input(INPUT_POST, $index,FILTER_SANITIZE_NUMBER_INT,FILTER_REQUIRE_ARRAY);
        if (!is_array($myIntArray)){
           $this->msg->addMessage($index, ucfirst("Not an Array"));
            //throw new \Exception("Must be Array");           
           $this->data[$index]=[];
           return $this;
        }
       
                        
        $this->data[$index]=$myIntArray;
        
        return $this;
    }
    
    function sanitizePhone($index){
        $this->sanitize($index);
        if ($this->hasErrors($index)){
             return $this;
        }
        $inputPhone = $this->data[$index];
        if (strlen(trim($inputPhone))<=0){    
            return $this;
        }
        
        try{
             $phoneUtil = PhoneNumberUtil::getInstance();
             $numberProto = $phoneUtil->parse($inputPhone, $this->countryCode);
             if ($phoneUtil->isValidNumber($numberProto)){
                $this->data[$index] = 
                 ($this->countryCode =='US')?$phoneUtil->format($numberProto, \libphonenumber\PhoneNumberFormat::NATIONAL):
                 $phoneUtil->format($numberProto, \libphonenumber\PhoneNumberFormat::INTERNATIONAL);
             }
             else{
                 $this->msg->addMessage($index, "Invalid phone number");
             }
                                
                
                    
            
            } catch (\Exception $ex) {
                $this->msg->addMessage($index, "Invalid phone number");
            //var_dump($ex->getMessage());
        }
        
        //
    }
    
    function get($index=null,$returnNull=true){
        if (is_null($index)){
            return $this->data;
        }
        
        if(key_exists($index, $this->data)){
            return $this->data[$index];
        }
        
        if ($returnNull){
            return null;
        }
        //if not returnNull
        throw new \Exception("Invalid use of function.  '$index' has not yet be tested for ".__LINE__);
    
    }
    
    /** 
     * @depresicated use get instead
     * @param type $index
     * @return type
     * @throws \Exception
     */
    function getSanitizedData($index=null){
        return $this->get($index);
    }
    
    function addMessage($index,$text){
        $this->msg->addMessage($index,$text);
    }
    function hasErrors($index=null){
       return $this->msg->hasMessage($index);
    
    }

    function getMessages(){
        return $this->msg;
    }

    function isBlank($index){
        if (key_exists($index, $this->data)){
            return !(strlen(trim($this->data[$index])));
        }
        else{
            return true;
        }
    }
    
    function removeMsg($index){
        $this->msg->removeText($index);
    }
    
    function removeSanitizedData($index){
        if (key_exists($index, $this->data));
            unset($this->data[$index]);
    }
    /**
     * @usage used to set or replace a value into the checker object that was 
     *          not sent from the user.
     * @param type $index - index of the value
     * @param type $value - value you want stored
     */
    function add($index, $value){
        $this->data[$index]=$value;
    }
    
    
    /* -------------- SETTERS/GETTERS ------------------------ */
    function isInputNonBlankValueRequired() {
        return $this->inputNonBlankValueRequired;
    }

    function setInputNonBlankValueRequired($inputNonBlankValueRequired) {
        $this->inputNonBlankValueRequired = $inputNonBlankValueRequired;
    }

    



    
}
