<?php
/**
 * Description of Security
 *
 * @author ken
 */
namespace Consulting247;

class Security {
   private $allowed=false;
   
    public function __construct($authority=null,$autoRedirect=true) {
       
        $this->allowed=true; 
        if (count($_SESSION)==0){
            if (count($_COOKIE)>0){
                foreach ($_COOKIE AS $index=>$value){
                    $_SESSION[$index] = $value;
                }
            }
            else{
                //if time expired
               if($autoRedirect){
                   (new SiteMap())->header($authority);     
               }
               else{
                  $this->allowed = false;
                  return;
               }
            }
        }
        
        if (!key_exists('authority', $_SESSION)){
            if ($autoRedirect){
               (new SiteMap())->header($_SESSION['authority']);     
           }
           else{    
               $this->allowed = false;
               return;
               
           }
        }
        
        if ($authority != $_SESSION['authority']){
           if ($autoRedirect){
               (new SiteMap())->header($_SESSION['authority']);     
           }
           else{    
               $this->allowed = false;
               return;
           }
        }
        
                
    }
    
    
    // getters
    public function getAllowed(){return $this->allowed;}

    
}
