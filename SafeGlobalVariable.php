<?php
namespace Consulting247;
use Consulting247\Message;
/**
 * Description of InputChecker
 *
 * @author ken
 */
namespace Consulting247;
use libphonenumber\PhoneNumberUtil;


//rename InputFilter ASAP or sooner
class SafeGlobalVariable{
    private $msg;
    private $data;
    private $inputType;
    private $countryCode;
    private $inputNonBlankValueRequired;
    
    function __construct($inputType = INPUT_POST){
        $this->msg = new Message();
        $this->inputType = $inputType;
        $this->data = [];
    }
    
    function getParamList(){
         $param = null;
         foreach (filter_input_array($this->inputType) AS $index=>$value){
              $param .="$index=$value&";
         }
         return "?".substr($param,0,-1);
    }

              
    function get($index, $santizeFilter = FILTER_SANITIZE_STRING, $validateFilter = null) {
          if (key_exists($index, $this->data)){
            return $this->data[$index];
        }
        switch ($santizeFilter){
            case(FILTER_SANITIZE_NUMBER_FLOAT):
            case(FILTER_SANITIZE_NUMBER_INT):
                $this->sanitizeNumber($index,$santizeFilter);
                break;
            case("INT_ARRAY"):
                $this->sanitizeIntArray($index);
                break;
            case("REQUIRE_ARRAY"):
                 $this->sanitizeArray($index);
                 break;
            case (FILTER_SANITIZE_EMAIL):
                 $this->sanitizeEmail($index);
                 break;

            case ("REQUIRE_PHONE"):
                 throw(new \Exception("not implemented"));
                 $this->sanitizePhone($index);
                 break;
            
            default :
                $this->sanitize($index,null,$santizeFilter);
        }
        return $this->data[$index];
    }//end get
    
    function exists($index){
        return (!is_null(filter_input($this->inputType, $index)));//,FILTER_DEFAULT,[FILTER_NULL_ON_FAILURE]));
        
    }
    
    function isEmpty($index){
        $tIndex = filter_input($this->inputType, $index,FILTER_SANITIZE_STRING);
        return empty($tIndex);//,FILTER_DEFAULT,[FILTER_NULL_ON_FAILURE]));
        
    }
    
    function sanitize($index,$stub=null,$sanitizeFilter=FILTER_SANITIZE_STRING){        
        $this->data[$index] = filter_input($this->inputType, $index,$sanitizeFilter);
        
        if (($this->inputNonBlankValueRequired) && 
            (strlen(trim($this->data[$index]))<=0)){
            $this->msg->addMessage($index, ucfirst(trim($stub." may not be blank")));
        }
        return $this;
    }

    function sanitizeArray($index,$stub=null){        
        $this->data[$index] = filter_input($this->inputType, $index,FILTER_SANITIZE_STRING,FILTER_REQUIRE_ARRAY);
        foreach ($this->data AS $key=>$value){
            if (($this->inputNonBlankValueRequired) && 
                (strlen(trim($value))<=0)){
                $this->msg->addMessage($index, ucfirst(trim($stub." may not be blank")));
            }
        }
        return $this;
    }
    
    function sanitizeEmail($index){
        //not working quite right yet what happens when email is not a required feild;
        $email = filter_input($this->inputType, $index,FILTER_SANITIZE_EMAIL);
       
        if ((!filter_var($email,FILTER_VALIDATE_EMAIL))){
            $this->addMessage($index, "Is not a valid email address");
        }       
        $this->data[$index]=$email;
        
        return $this;
    }
    
    function sanitizeURL($index){
        $url = filter_input($this->inputType, $index,FILTER_SANITIZE_URL);
        if (is_null($url)){
            $this->addMessage($index, "URL may not be blank");
            $this->data[$index]=$url;
            return $this;
        }
       
        //make sure that the component works
        if ((substr($url,0,7) !=="http://") &&
            (substr($url,0,8) !=="https://")){
            $url = "http://".$url;       
        }

        //is in Proper form
        if (filter_var($url, FILTER_VALIDATE_URL)){
            //see if host is valid
            $urlComponents= parse_url($url);
            
            $i1 = (key_exists("host", $urlComponents))?"host":"path";
            $urlPathArray = explode('/', $urlComponents[$i1]);
       
            //is the host good
            $this->urlIPAddress = filter_var(gethostbyname($urlPathArray[0]),FILTER_VALIDATE_IP);
            if (filter_var($this->urlIPAddress,FILTER_VALIDATE_IP) &&
                ($this->urlIPAddress !=="92.242.140.21")){
                //92.242.140.21 is a redirection service 
                $headers= get_headers($url);
                if(strpos($headers[0],'200')===false){
                    //var_dump($headers);
                    $this->urlHeaders = $headers;
                    $this->addMessage($index, "Page cannot be read");            
                }
            }
            else{
                 $this->addMessage($index, "URL Host can not locate");
            }
        }
        else{
            $this->addMessage($index, "URL is not set valid");
        }
        $this->data[$index]=$url;
        
        return $this;
    }
    
    private function sanitizeNumber($index,$filter=FILTER_SANITIZE_NUMBER_INT){
        //not working quite right yet ;
        switch($filter){
            case(FILTER_SANITIZE_NUMBER_FLOAT):
                $myInt = filter_input($this->inputType, $index,$filter,FILTER_FLAG_ALLOW_FRACTION);
                break;
            default :
                $myInt = filter_input($this->inputType, $index,$filter);
                break;
        }
             
        if (($this->inputNonBlankValueRequired) && 
            (strlen(trim($myInt))<=0)){
            $this->sanitize($index);
            $this->msg->addMessage($index, ucfirst("'{$this->data[$index]}' is not valid"));
            return $this;
        }
        /*
        switch($filter){
            case(FILTER_SANITIZE_NUMBER_FLOAT):
                $this->data[$index]= floatval($myInt);
                break;
            default :
                $this->data[$index]=  intval($myInt);
                break;
        }
         * 
         */
        $this->data[$index]=$myInt;
        return $this;
    }
    
    function sanitizeIntArray($index){
        $myIntArray = filter_input(INPUT_POST, $index,FILTER_SANITIZE_NUMBER_INT,FILTER_REQUIRE_ARRAY);
        if (!is_array($myIntArray)){
           $this->msg->addMessage($index, ucfirst("Not an Array"));
            //throw new \Exception("Must be Array");           
           $this->data[$index]=[];
           return $this;
        }
       
                        
        $this->data[$index]=$myIntArray;
        
        return $this;
    }
    
    function sanitizePhone($index){
        $this->sanitize($index);
        if ($this->hasErrors($index)){
             return $this;
        }
        $inputPhone = $this->data[$index];
        if (strlen(trim($inputPhone))<=0){    
            return $this;
        }
        
        try{
             $phoneUtil = PhoneNumberUtil::getInstance();
             $numberProto = $phoneUtil->parse($inputPhone, $this->countryCode);
             if ($phoneUtil->isValidNumber($numberProto)){
                $this->data[$index] = 
                 ($this->countryCode =='US')?$phoneUtil->format($numberProto, \libphonenumber\PhoneNumberFormat::NATIONAL):
                 $phoneUtil->format($numberProto, \libphonenumber\PhoneNumberFormat::INTERNATIONAL);
             }
             else{
                 $this->msg->addMessage($index, "Invalid phone number");
             }
                                
                
                    
            
            } catch (\Exception $ex) {
                $this->msg->addMessage($index, "Invalid phone number");
            //var_dump($ex->getMessage());
        }
        
        //
    }
    

       
    function addMessage($index,$text){
        $this->msg->addMessage($index,$text);
    }
    function hasErrors($index=null){
       return $this->msg->hasMessage($index);
    
    }

    function  getErrorsAsArray(){
         
         return $this->getMessages()->getText();
    }
         
    function getError($index){
         return $this->getMessage($index);
    }

    function getMessage($index){
        if (key_exists($index, $this->msg)){
             return $this->msg[$index];
        }
        return null;
    }    
    
    
    function getMessages(){
        return $this->msg;
    }

    function isBlank($index){
        if (key_exists($index, $this->data)){
            return !(strlen(trim($this->data[$index])));
        }
        else{
            return true;
        }
    }
    
    function removeMsg($index){
        $this->msg->removeText($index);
    }
    
    function removeSanitizedData($index){
        if (key_exists($index, $this->data));
            unset($this->data[$index]);
    }
    /**
     * @usage used to set or replace a value into the checker object that was 
     *          not sent from the user.
     * @param type $index - index of the value
     * @param type $value - value you want stored
     */
    function add($index, $value){
        $this->data[$index]=$value;
    }
    
    
    /* -------------- SETTERS/GETTERS ------------------------ */
    function isInputNonBlankValueRequired() {
        return $this->inputNonBlankValueRequired;
    }

    function setInputNonBlankValueRequired($inputNonBlankValueRequired) {
        $this->inputNonBlankValueRequired = $inputNonBlankValueRequired;
    }

    function getData(){
        return $this->data;
    }

    function getCount(){
         return(count($this->data));
    }

    
}
