<?php
namespace Consulting247\Mailer;
interface MGKeyInterface {
   
    function getDomain();
    function getFrom();
    function getKey($isProduction=true);
   
}
