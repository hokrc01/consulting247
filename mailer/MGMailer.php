<?php
namespace Consulting247\Mailer;

use Mailgun\Mailgun;
class MGMailer{
        private $mgKey;
        
	private $subject;
	private $replyTo;
	private $messageHTML;
	private $attachment;
	private $to;
        private $batchSize=999;
	
	
	//does function user have access to this routine?
	function __construct(MGKeyInterface $mgKey) {
            $this->mgKey = $mgKey;
	}
    private function batchSend($recipientVariableSet){
        
        
       # Instantiate the client.
        $mgClient = new Mailgun($this->mgKey->getKey());
        $domain = $this->mgKey->getDomain();

        $addressee =['from' => $this->mgKey->getFrom(),
                     'subject' => $this->subject,
                     'h:reply-to'=>$this->getReplyTo(),	
                     'html'=> $this->messageHTML
                    ];

        
        foreach ($recipientVariableSet AS $key=>$recipient){
            $addressee['to']= implode(",",array_keys($recipient));
            $addressee['recipient-variables'] = json_encode($recipient);

            //var_dump($this->attachment);	exit;
			
            if (is_null($this->attachment)){
                $result[]=array(
                            $mgClient->sendMessage($domain, $addressee),
                            "batch_count"=>count($recipient)
                          );
            }
            else{
               	//var_dump($this->attachment);	exit;
			
                $result[]=array(
                            $mgClient->sendMessage($domain, 
                                $addressee,
                                ['attachment' => $this->attachment]),
                            "batch_count"=>count($recipient)
                          );
            }

        }
        return $result;
    }//end function

    function send($fromList=Array()){
            if (count($fromList)>0){
                return $this->batchSend($fromList);
            }

            //echo "sending</br>";
		//echo 'start send </br>';
		if (is_null($this->getReplyTo()) || 
			is_null($this->getMessageHTML()) ||
   			is_null($this->getSubject()) || 
			is_null($this->getTo())){
			throw new Exception('Must have Reply-to to,subject and Message');
		}
		else{
			# Instantiate the client.
			$mgClient = new Mailgun($this->mgKey->getKey());
			$domain = $this->mgKey->getDomain();
			$from = $this->mgKey->getFrom();

                        
			try{
    			if (is_null($this->getAttachment())){
					$result = $mgClient->sendMessage($domain, array( 
						'from' => $from, 
						'to' => $this->getTo(), 
						'h:reply-to'=>$this->getReplyTo(),	
						'subject' => $this->getSubject(), 
						'html' => $this->getMessageHTML(),
						));
				}
				else{
				# Make the call to the client. with attachment
					$result = $mgClient->sendMessage($domain, array( 
						'from' => $from, 
						'to' => $this->getTo(), 
						'h:reply-to'=>$this->getReplyTo(),	
						'subject' => $this->getSubject(), 
						'html' => $this->getMessageHTML(),
						),
							array( 
							'attachment' => $this->getAttachment()
								//'uploads/Test_Page.pdf') 
					));
					
				}
		}
		catch (Exception $e){
			throw $e;
		}//end catch
		}
		//echo "end send</br>";
          
         //*/
	}
	
	
	/*  -- setters and getter --*/


	public function getSubject() {
		return $this->subject;
	}

	public function setSubject($subject) {
		$this->subject = $subject;
	}

	public function getReplyTo() {
		return $this->replyTo;
	}

	public function setReplyTo($name,$email) {
		if (is_null($name)){
			$this->replyTo = $email;
		}
		elseif ((trim($email)==='') || is_null($email)){
			throw  new Exception ("<br/>'$email' is not a valid email address</br>");
		}
		else{
			
			$this->replyTo = "$name <$email>";
			//echo 'here'."$email '{$this->replyTo}' </br>";
		}

		//echo "'$this->replyTo'";
		
		//$this->replyTo = 'master fred flinstone <hokrc01@gmail.com>';
		
	}

	function loadExternalEmailText($filename,$infoArray1=null,$infoArray2=null){
			
		if (file_exists($filename)){
			ob_start();
			include $filename;
			$textCopy = ob_get_clean();
			$this->setMessageHTML($textCopy);
			return $textCopy;
		}
		
		return false;
	}






	public function getMessageHTML() {
		return $this->messageHTML;
	}

	public function setMessageHTML($messageText) {
		$this->messageHTML = $messageText;
	}

	public function getAttachment() {
		return $this->attachment;
	}

	public function addAttachment($attachment) {
		$this->attachment[] = $attachment;
	}

	public function getTo() {
		return $this->to;
	}

	public function setTo($to) {
		$this->to = $to;
	}

        function getBatchSize() {
            return $this->batchSize;
        }

        function setBatchSize($batchSize) {
            $this->batchSize = $batchSize;
        }

}//end class


?>