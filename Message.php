<?php

/**
 * Description of Message
 *
 * @author ken
 */
namespace Consulting247;
class Message {
	//put your code here
	private $text=array();
	private $htmlClass;
	
	public function __construct() {
		$this->setHtmlClass('error');
	}
	
        public function addMessage($index,$text){
            $this->text[$index] = $text;
        }
        
        public function getMessage($index){
            if (is_null($index)){
                return $this->text;
            }
            
            if (key_exists($index, $this->text)){
                return $this->text[$index];
            }

            return null;

        }
        
        public function noMessages(){
            return (count($this->text)==0);
        }
        
	public function getText($id=null) {
		if (is_null($id))
			return $this->text;
		else{
			if (array_key_exists($id, $this->text)){
				return $this->text[$id];
			}
			else{
				return null;
			}
		}
	}

	public function addText($text,$key=null) {
		if ($key){
			$this->text[$key] = $text;
		}
		else{
			$this->text[] = $text;
		}
	}

	public function count(){
		return count($this->text);
	}
	public function get($index=null,$cssId=null){
		if (is_null($cssId)){
			$elementId = null;
		}else{
			$elementId = "id=\"$cssId\"";
		}
		
		//C247::showVar($this->text);
		if (array_key_exists($index, $this->text)){
			$ret_val = "<div $elementId class=\"$this->htmlClass error-$index msg-$index\">{$this->text[$index]}</div>";
		}
		else{
			if (is_null($index)){
				$ret_val = "<div $elementId class=\"$this->htmlClass error-$index msg-$index\">&nbsp;</div>";
			}
			else
				$ret_val = null;
		}
		return $ret_val;
	}
	
	
	public function getHtmlClass() {
		return $this->htmlClass;
	}

	public function setHtmlClass($htmlClass) {
		$this->htmlClass = $htmlClass;
	}

	public function format(){
		//$ret_val = "<div class=\"message\">";
		$ret_val=null;
		if ($this->text)foreach ($this->text AS $index=>$line){
		$ret_val .= "<p class=\"$this->htmlClass error-$index msg-$index\">$line</p>";
		}
		else{
			$ret_val="";
		}
		//$ret_val .="</div>";
		return $ret_val;
	}
	

	public function hasMessages(){
            return (bool) (count($this->text)>0);
        }
	public function hasMessage($index=null){
		if (is_null($index)){
			return count($this->text);
                }
		else{
			return array_key_exists($index, $this->text);
			
		}
	}
	public function __toString() {
		$ret_val = "text value :";
		foreach ($this->text AS $key=>$t){
			$ret_val .= "<p>$t</p>";
		}
		return $ret_val;
	}
	
	public function removeText($index){
		if (key_exists($index, $this->text)){
			unset($this->text[$index]);
		}
		
	}

}

?>
