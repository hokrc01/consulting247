<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Consulting247\PostalCode;

class PostalCode {
    private $codeFormat;
    private $countryCode;
    function __construct($countryCode) {
        $this->countryCode=$countryCode;

        
        ob_start();                      // start capturing output
        include('cldr.inc');   // execute the file
        $content = ob_get_contents();    // get the contents from the buffer
        ob_end_clean();   
               

        foreach(preg_split("/((\r?\n)|(\r\n?))/", $content) as $line){
            $t = explode(", ",  str_replace('"','', $line));
            if ($t[0] == $this->countryCode){
                $this->codeFormat = trim($t[1]);
                return;
            }
        }
        $this->codeFormat = null;
    }

    function isValidFormat($pcValue){
        //specail case testng
         switch($this->countryCode){
             case("US"):
                return (bool) $this->isValidUSFormat($pcValue);
                break;
                
             default:
                if (is_null($this->codeFormat)){
                    return true;
                }
                 
                return (bool) preg_match('/'.$this->codeFormat.'/',$pcValue);
                                         
         }
          
        
        
    }
 
    private function isValidUSFormat($pcValue){
        //mujst be in 5 diget or 99999-9999 format
        if (strlen($pcValue)==5){
            return preg_match("/[0-9]{5}/",$pcValue);
            
        }
                 
        if (strlen($pcValue)==10){
            return preg_match("/[0-9]{5}[ -][0-9]{4}/",$pcValue);
        }
        
        return false;
    }


    /** setters/getters */

}   
 