<?php
/**
 * Description of Session
 * Session is a wrapper class for the $_SESSION[$this->sessionIndex] Variable
 *
 * @author ken
 */
namespace Consulting247;
class Session {
    private $sessionIndex;
    

    function __construct($sessionIndex='data') {
        $this->sessionIndex = $sessionIndex;    
        
        if (!key_exists($sessionIndex, $_SESSION)){
            $_SESSION[$sessionIndex]=[];
        }
    
    }
    
    
    function set($index,$value){
        $_SESSION[$this->sessionIndex][$index]=$value;
    }
    
    /**
     * 
     * @param type $index - storage $_SESSION[$this->sessionIndex][$index]
     * @param type $returnType - type of emply value to return 
     * @return type - data saved at index or null if no value;
     */
function get($index=null,$returnType=null){
        if (is_null($index)){
            return $_SESSION[$this->sessionIndex];
        }
        
        if (key_exists($index, $_SESSION[$this->sessionIndex])){
            return $_SESSION[$this->sessionIndex][$index];
        }

        switch($returnType){
            case('array'):
                return [];
              
            default :
                return null;
        }
    }
    /**
     * 
     * @param type $index - storage $_SESSION[$this->sessionIndex][$index]
     * @param type $returnType - type of emply value to return 
     * @return type - data saved at index or null if no value;
     */
    function getAndRemove($key,$returnType=null){
        $temp = $this->get($key,$returnType);
        $this->remove($key);
        return $temp;
    }
    
    function exists($index){
        return key_exists($index, $_SESSION[$this->sessionIndex]);
    }
    
    function remove($index){
        if (key_exists($index, $_SESSION[$this->sessionIndex])){
            unset($_SESSION[$this->sessionIndex][$index]);
        }
        else{
            //do nothing
        }
    }
    function removeAll(){
        $_SESSION[$this->sessionIndex]=[];
    }
}
