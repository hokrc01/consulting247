<?php
namespace Consulting247;
require_once 'vendor/autoload.php';

class Constants {
    private $constants;
    /**
     * 
     * @param type $type where is the category of constant stored in the database
     */
    function __construct($type = null) {
        $db=new Database();
        if (is_null($type)){
            $sql = "SELECT * FROM constants";
            $param = [];
        }
        else{
            $sql = "SELECT * FROM constants WHERE type= :type";
            $param=["type"=>$type];
        }
        $dataSet = $db->select($sql,$param);
        if (!empty($dataSet)){
            foreach($dataSet AS $key=>$row){
                $this->constants[$row['name']]=$row['value'];
            }
        }
    }    
    
    function getConstant($index){
        if (key_exists($index, $this->constants)){
            return $this->constants[$index];
        }
        
        return null;
    }
}
