<?php
namespace Consulting247;
class Telephone {
    private $areaCode;
    private $ext;
    private $number;
	
    function __construct($string) {
        if (is_null(trim($string))){
            return;
        }
		
	$pattern = '/[^0-9]*/';
	$numbersOnly = preg_replace($pattern,'', $string);
	
	$numberOfDigits = strlen($numbersOnly);
	if ($numberOfDigits == 10) {	        
            $this->areaCode = substr($numbersOnly, 0,3);
            $this->ext      = substr($numbersOnly, 3,3);
            $this->number   = substr($numbersOnly, 6,4);
	}
	else {
            throw new \Exception('(###) ###-#### format required');
	}
    }
	
    function format(){
        if (is_null($this->areaCode)){
            return null;
        }
        return "($this->areaCode) $this->ext - $this->number";
    }
	
    function formatTwilio(){
         if (is_null($this->areaCode)){
            return null;
        }
        return "+1{$this->areaCode}{$this->ext}{$this->number}";
    }   

    
    function __toString() {
        return $this->format();
    }
}