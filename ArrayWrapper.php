<?php
namespace Consulting247;

/**
 * Description of ArrayWrapper
 *
 * @author ken
 */
class ArrayWrapper {
     private $data;
     function __construct($array=[]) {
          $this->data=$array;
     }
     function set($index,$value){
          $this->data[$index]=$value;
     }
     function get($index){
          if (key_exists($index, $this->data)){
               return $this->data[$index];
          }
          return null;
     }
     
     function getAll(){
          return $this->data;
     }
     
     function count(){
          return count($this->data);
     }
     /**
      * @uses contains will check to see if the needle is in @ the index or if element is array will go one level deeper
      *
      * @param type $index
      * @param type $needle
      * @return type
      */
     function contains($index, $needle){
          if (is_array($this->get($index))){
               return array_search($needle, $this->data[$index]);                             
          }
          else{
               //no sure what to do
               return array_search($needle, $this->data);
          }
     }
}
