<?php
namespace Consulting247\Database;
class DataWrapper {

     //should be private, but already have lots of code out there
     protected $data = [];
     protected $errorText = [];

     function __construct($dataSet = null) {
          $this->setAll($dataSet);
     }
     
     function set($propertyName, $value) {
          $this->data[$propertyName] = $value;
          return $this;
     }

     function setAll(Array $dataSet) {
          $this->data = $dataSet;
          return $this;
     }

     function setNext($value) {
          $this->data[] = $value;
          return $this;
     }

     function load(array $arrayOfData){
          foreach ($arrayOfData AS $property=>$value){
               $this->set($property, $value);
          }
     }
     
     function size() {
          return count($this->data);
     }

     function get($propertyName) {
          if (key_exists($propertyName, $this->data)) {
               return $this->data[$propertyName];
          }
          return null;
     }

     function getAll() {
          return $this->data;
     }

     function removeAll() {
          $this->data = [];
          $this->errorText = [];
          return $this;
     }

     function remove($index = null) {
          if (key_exists($index, $this->data)) {
               unset($this->data[$index]);
          }
          if (key_exists($index, $this->errorText)) {
               unset($this->errorText[$index]);
          }
          return $this;
     }

     function setError($propertyName, $errorText = "Error") {
          $this->errorText[$propertyName] = $errorText;
          return $this;
     }
     
     function addErrors(array $errors){
          if (empty($this->errorText)){
               $this->errorText = $errors;
          }
          else{
               foreach ($errors AS $propertyName=>$errorText){
                    $this->setError($propertyName, $errorText);
               }
          }
          return $this;
     }

     function getError($propertyName) {
          if (key_exists($propertyName, $this->errorText)) {
               return $this->errorText[$propertyName];
          }
          return null;
     }

     function getErrorAll() {
          return $this->errorText;
     }

     function removeError($propertyName) {
          if (key_exists($propertyName, $this->errorText)) {
               unset($this->errorText[$propertyName]);
          }
     }

     function isEmpty() {
          return (count($this->data) <= 0);
     }

     function hasErrors($propertyName = null) {
          if (is_null($propertyName)) {
               return !empty($this->errorText);
          } else {
               return (bool) key_exists($propertyName, $this->errorText);
          }
     }

}
