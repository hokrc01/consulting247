<?php
namespace Consulting247\Database;
use PDO;

class Database{
    private $dbh;
    private $effectedRows;
    
    public function __construct(){
        // I need a way  to secure this routine
        if (true){
           // Set options
            $options = array(
                PDO::ATTR_ERRMODE=> PDO::ERRMODE_EXCEPTION,
                PDO::MYSQL_ATTR_INIT_COMMAND => 'SET SESSION SQL_BIG_SELECTS=1'
                            );
            // Create a new PDO instanace
            $this->dbh = new PDO(DSN, DBUSER, DBPASS, $options);
            //throw mistake no catch necessary
            
             // Catch any errors
            
        }
        else{
            throw new Exception("Illegal use of system", "c247-1235");
            exit;
        }
   }

    public function select($sql, array $params=array()){
        $stmt = $this->dbh->prepare($sql);
        $stmt->execute($params);	
        $retVal = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $retVal;
   }
    public function insert($tableName,$data, $onDuplicateKeyStmt=null) {
        
       //standard Insert 
       $cmd = "INSERT INTO $tableName (".
           implode(",",array_keys($data)).") VALUES (:".
           implode(",:", array_keys($data)).")";
       
       if (!is_null($onDuplicateKeyStmt)){
           $cmd .= " ON DUPLICATE KEY $onDuplicateKeyStmt";
       }
       //echo "$cmd";
       $insertStatement = $this->dbh->prepare($cmd);
       $insertStatement->execute($data);
       return $this->dbh->lastInsertId();
       

   }
   /**
    * @usage $multi data must be correct form $array[0]['dataArray'] to work
    
    * @param type $tableName name of table 
    * @param type $multiData multi dementional array of the form $x[index]= dataArray[]
    * @return type
    */
   function insertMulti($tableName,$multiData){
        $cmd = "INSERT INTO $tableName ("
            .implode(",",array_keys($multiData[0])).") VALUES ";
    
    
        foreach ($multiData AS $row){
            $cmd .= '('  . implode(",",array_fill(0,sizeof($row),"?")) . '),';
            foreach ($row AS $key=>$value){
                $newData[] = $value;
            }
        }
    
        $cmd = substr($cmd, 0,-1);
    
        $insertStatement = $this->dbh->prepare($cmd);
        $insertStatement->execute($newData);
        return $this->dbh->lastInsertId();    
        
   }
   
     public function update($tableName,$idArray,$data, Array $inColumn=[]) {
       
       $cmd = "UPDATE $tableName SET ";
       foreach ($data AS $index=>$value){
           $cmd .= " $index = :$index,";
       } 
       $cmd = substr($cmd, 0,-1);
       $cmd .=" WHERE ";
       
       if (empty($inColumn)){
           $cmd .= '1 AND ';
       }
       else{           
           foreach ($inColumn AS $colName=>$valueArray){
              $cmd .= "$colName IN (". implode(',',$valueArray).") AND ";
                   
               
           }
        }
       
       foreach($idArray AS $index=>$value){
           $cmd.= " $index = :$index AND ";
           $data[$index]=$value;
       }
       $cmd = substr($cmd, 0,-4);       
       
       $updateStatement = $this->dbh->prepare($cmd);
       try{
           $updateStatement->execute($data);
       }
       catch (\Exception $e){
           echo "$cmd";
           throw $e;
       }
        
   }
   
   public function delete($tableName, $where){
       $cmd = "DELETE FROM $tableName WHERE";
       foreach($where AS $index=>$value){
           $cmd.= " $index = :$index AND";
           $data[$index]=$value;
       }
       $cmd = substr($cmd, 0,-3);       
       
       $deleteStatement = $this->dbh->prepare($cmd);
       $deleteStatement->execute($data);
   }
      /**
    * @uses element Try not to use this methodif possible
    * @param type $sql a valid SQL statement
    * @param type $params parameters
    */
   public function execute($sql,$params=null){
        $stmt = $this->dbh->prepare($sql);
        if (is_null($params)){
            $rv = $stmt->execute();
        }else{
            $rv = $stmt->execute($params);
        }
        $this->effectedRows = $stmt->rowCount();
        return $rv;
   }

   function getRowCount(){
        return $this->effectedRows;    ;
   }
}//end class

