<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Consulting247\Database;

class Parameter {

     private $columnName;
     private $value;
     private $operator;
     private $placeholderName;
     private $conjunction;

     function __construct($columnName,$value,$operator = '=', $placeholderName=null, $conjunction = "AND") {
          $this->columnName = $columnName;
          $this->value = $value;
          $this->operator = $operator;         
          if (is_null($placeholderName)){
               $this->placeholderName = $columnName;
          }
          else{
               $this->placeholderName = $placeholderName;
          }
          $this->conjunction = $conjunction;
     }

     function __toString() {
          return "$this->conjunction $this->columnName $this->operator :$this->placeholderName";
     }
     
     function getValue(){
          return $this->value;
     }
     
     function getValueIndex(){
          return $this->placeholderName;
     }
     
     

}
