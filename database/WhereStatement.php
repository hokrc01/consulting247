<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Consulting247\Database;
use Consulting247\Database\Parameter;
abstract class WhereStatement {
     private $params;
     private $whereStatement;

     function __construct($params = []) {
          $this->whereStatement = " 1";
          if (empty($params)){
               $this->params = $params; 
               return;
          }          
          
          //else loop
          foreach ($params AS $parameter) {
               $this->whereStatement  .= ' '.$parameter; 
               $this->params[$parameter->getValueIndex()]=$parameter->getValue();
          }
        
     }
     
     function getParams(){
          return $this->params;
     }
     
     function getWhereStatement(){
          return $this->whereStatement;
     }
     

}
