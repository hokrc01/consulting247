<?php
namespace Consulting247;

class EmailAddress{
    private $name;
    private $email;
    /**
     * 
     * @param type $name
     * @param type $email Make sure that $email is VALID before using function
     */
    function __construct($name =null, $email=null) {
       $this->setName($name);
       $this->setEmail($email);
    }
    
    //setters
    
    function setName($name) {
        $this->name = $name;
        return $this;
    }

    //valid email address
    function setEmail($email) {
       $this->email = $email;
       return $this;
    }
    
    public function __toString() {
       
       return $this->get();
   }
   public function get(){
       return "$this->name <$this->email>";
   }

}
