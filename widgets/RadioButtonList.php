<?php

namespace Consulting247\Widgets;

class RadioButtonList extends HTMLWidget {

     private $type = "radio";
     private $choices;
     private $radioOnRight = true;

     function render() {
          $widget = ($this->li)?'<li class="input-wrapper'." $this->widgetClass-li". '">':null;
          $widget .= (isset($this->label))?"<label class=\"$this->widgetClass-label\">$this->label</label>":null;
          
          if (is_null($this->class)){$this->class = $this->name;}

          $id = (is_null($this->id)) ? null : "id = \"{$this->id}-wrapper\"";

          $widget .= "<div $id class=\"$this->class input-div input-rb-list $this->widgetClass-outer\">";
          $widget .= '<div class="choice-wrapper '.$this->widgetClass.'-inner">';
          
          foreach ($this->choices AS $value => $label) {
               $selected = ($value == $this->value) ? "checked=checked" : null;

               if ($this->radioOnRight) {
                    $widget .= "<div class=\"sublabel sublabel-$value $this->widgetClass-sublabel-div $this->widgetClass-$value-sublabel-div\">"
                         . "<label class=\"c247-rb-label $this->widgetClass-sublabel-label $this->widgetClass-$value-label\">$label</label>"
                         . "<input type=\"{$this->type}\" id=\"$this->id\" class=\"$this->class-rb $this->widgetClass-$value-input\" name=\"$this->name\" value=\"$value\" $selected $this->otherProperties />"
                         . "</div>";
               } else {
                  $widget .= "<div class=\"sublabel sublabel-$value $this->widgetClass-sublabel-div $this->widgetClass-sublabel-value-sublabel-div>\">"
                      . "<label id=\"id-$this->id-sublabel-$value\" "
                       . "class=\"sublabel-label sublabel-label-$value $this->widgetClass-sublabel-label $this->widgetClass-sublabel-$value-label\">"
                      . "<input type=\"{$this->type}\" id=\"$this->id\" class=\"$this->class-rb $this->widgetClass-input\" name=\"$this->name\" value=\"$value\" $selected $this->otherProperties />"
                      . "<div id=\"$this->id-$value\" class=\"c247-rb-label $value-label $this->id-label $this->widgetClass-$value-label\">$label</div>"
                      . "</label>"
                      . "</div>";
               }
          }
          $widget .= "</div>";
          if (strlen(trim($this->errorMsg)) > 0) {
               $widget .= '<div class="input-error error error-1 ' . $this->class . 'Error">' .
                    $this->errorMsg .
                    '</div>';
          }
          $widget .= '</div>';
          if ($this->li)
               $widget .= '</li>';
//var_dump($widget);
          echo $widget;
     }

     //
     //
    ////end render
     //setters


     function addChoice($value, $labelText) {
          $this->choices[$value] = $labelText;
          return $this;
     }

     /**
      * 
      * @param array $choiceArray of the form [$value]=$label,]
      * @return \Consulting247\Widgets\CheckboxList
      */
     function setChoices($choiceArray) {
          $this->choices = $choiceArray;
          return $this;
     }

     function isLabelBeforeRadio() {
          return $this->radioOnRight;
     }

     function setLabelBeforeRadio($radioOnRight = true) {
          $this->radioOnRight = $radioOnRight;
          return $this;
     }

}
