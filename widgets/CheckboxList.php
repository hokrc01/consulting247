<?php
namespace Consulting247\Widgets;

class CheckboxList extends HTMLWidget {
    private $type="checkbox";
    private $choices;
        
    function render(){
       $widget = '';
        if ($this->li) {$widget .= '<li class="input-wrapper">';}
         
        if (isset($this->label)){$widget .= "<label id=\"$this->id-title\" class=\"$this->widgetClass-label\">$this->label</label>";}
		
        if (is_null($this->class)) {$this->class=$this->name;}
        
	$widget .= "<ul id=\"$this->id\" class=\"no-list-style input-div input-cb-list $this->class $this->widgetClass-ul\">";
        foreach ($this->choices AS $value=>$label){
            if (is_array($this->value)){
                $selected = (in_array($value, $this->value))?"checked=checked":null;
            }else{
                $selected='';
            }
            
            
            $widget .= "<li id=\"$this->id-$value-li\" class=\"checkbox-li $this->widgetClass-li $this->widgetClass-$value-li\">"
                . "<input type=\"{$this->type}\"  "
                . "class =\"cb-$this->class $this->widgetClass-cb $this->widgetClass-$value-cb\" "
                    . 'name="'.$this->name."[$value]\" value=\"$value\" $selected />"
                . "<label class=\"$this->widgetClass-$value-label\">$label</label>"
                . "</li>";
        }
        $widget .= '</ul>';
        if (strlen(trim($this->errorMsg))>0){
            $widget .= '<div class="input-error error-1 '.$this->class.'Error">'.
                        $this->errorMsg.
                        '</div>';
        }
        if ($this->li){$widget .= '</li>';}
        echo $widget;
    }
    //
    //
    ////end render
        //setters
    
 
    function addChoice($value,$labelText){
        $this->choices[$value] = $labelText;
        return $this;
    
    }
    /**
     * 
     * @param array $choiceArray of the form [$value]=$label,]
     * @return \Consulting247\Widgets\CheckboxList
     */
    function setChoices($choiceArray){
        $this->choices = $choiceArray;
         return $this;
    }

}