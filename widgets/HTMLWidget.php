<?php

namespace Consulting247\Widget;

/**
 * Description of HTMLWidget
 *
 * @author ken
 */

namespace Consulting247\Widgets;

abstract class HTMLWidget {

     protected $name;
     protected $id;
     protected $class;
     protected $label;
     protected $value;
     protected $errorMsg;
     protected $li = true;
     protected $liClass;
     protected $otherProperties = null;
     protected $widgetClass = null;
     protected $errorClass = false;
     
     abstract function render();

     // setters
     function setName($name) {
          $this->name = $name;
          return $this;
     }

     function setId($id) {
          $this->id = $id;
          return $this;
     }

     function setClass($class) {
          $this->class = $class;
          return $this;
     }

     function setLabel($label) {
          $this->label = $label;
          return $this;
     }

     function setValue($value) {
          $this->value = $value;
          return $this;
     }

     function setErrorMsg($errorMsg) {
          $this->errorMsg = $errorMsg;
          return $this;
     }
     function setErrorClass($errorClass){
          $this->errorClass = $errorClass;
          return $this;
     }

     function setLi($li) {
          $this->li = $li;
          return $this;
     }

     function setLiClass($liClass) {
          $this->liClass = $liClass;
          return $this;
     }

     function setOtherProperties($prop) {
          $this->otherProperties = $prop;
          return $this;
     }

     function setWidgetClass($widgetClass) {
          $this->widgetClass = $widgetClass;
          return $this;
     }

}
