<?php

namespace Consulting247\Widgets\FileUploader;

use Consulting247\Widgets\HTMLWidget;

class FileUploaderWidget extends HTMLWidget {

     private $fileNames = [];

     function __construct($inputTagName = "upload") {
          $this->id = $inputTagName;
          $this->class = $inputTagName;
          $this->name = $inputTagName;
     }

     function render() {

          echo '<ul id="' . $this->id . '-ul" class="no-list-style ' . $this->class . '-ul">';
          echo $this->getInputTypeFile();
          echo "</ul>";
          echo '<div class="menu-div">'
          . '<button class="add-' . $this->class . '-btn">Add Another File</button>'
          . '</div>';

          echo "<script>"
          . "//located in Consulitng247\Widgets\FileUploader\FileUploaderWidget.php"
          . "\n\r"
          . '$(document).ready(function () {
         $(".add-' . $this->class . '-btn").click(function (event) {
             event.preventDefault();
             $("#' . $this->id . '-ul").append('
          . "'{$this->getInputTypeFile()}');"
          . '});

         $("#' . $this->id . '-ul").on("click",".remove-' . $this->class . '-btn",function (event) {
             event.preventDefault();
             $(this).closest(".' . $this->class . '-li").remove();
         });
     });'
          . "</script>";
     }

     private function getInputTypeFile() {
          $html = '<li class="' . $this->class . '-li">'
               . '<input type="file" name="' . $this->name . '[]"'
               . ' class="' . $this->class . '">'
               . '<button class="remove-' . $this->class . '-btn">Unattach</button>'
               . '</li>';
          return $html;
     }

     function renderFormOpenTag($action, $id = null, $class = null) {
          //in order to upload a file it most be "POST" method
          echo '<form action="' . $action . '" method="post"
          id="' . $id . '" class="' . $class . '"
          enctype="multipart/form-data" >';
     }

     /**
      * @usage use instead of </form> tag
      */
     function renderFormCloseTag() {
          echo '</form>';
     }

}
