<?php

namespace Consulting247\Widgets\FileUploader;

class FileUploaderManager {

     private $fileList;
     private $targetDirName;
     private $sizeLimit;

     function __construct($targetDirName, $sizeLimit = 500000) {
          $this->sizeLimit = $sizeLimit;
          $this->fileList = [];

          $this->targetDirName = $targetDirName;

          foreach ($_FILES AS $postIndex => $fileDescription) {
               if (is_array($fileDescription['name'])) {
                    $this->processMulti($fileDescription);
               } else {
                    $this->uploadFile($fileDescription);
               }
          }
     }

     private function processMulti($fileDescription) {
          $keyArray = array_keys($fileDescription);
          foreach ($fileDescription['name'] AS $fileCnt => $value) {
               $singleFileDescription = [];
               foreach ($keyArray AS $index) {
//echo " $index $fileCnt $value $index<br>";
                    $singleFileDescription[$index] = $fileDescription[$index][$fileCnt];
               }
               $this->uploadFile($singleFileDescription);
          }
     }

     private function uploadFile($fileDescription) {
          if ($this->hasFileError($fileDescription['error'])){
              return false;
          }

          $msg=null;


          $target_file = trim($this->targetDirName) . '/' . basename($fileDescription["name"]);
          $uploadOk = true;
//    $imageFileType = pathinfo($target_file, PATHINFO_EXTENSION);
// Check if file already exists
          if (file_exists($target_file)) {
               $msg.= "Sorry, file '$target_file' already exists.  ";
               $uploadOk = false;
          }

          if ($fileDescription["size"] > $this->sizeLimit) {
               $msg = "Sorry, your file is too large.";
               $uploadOk = false;
          }

// Check if $uploadOk is set to 0 by an error
          if ($uploadOk) {
               if (move_uploaded_file($fileDescription["tmp_name"], $target_file)) {
//$msg .=" The file " . basename($fileDescription["name"]) . " has been uploaded.";
                    $this->fileList[] = $target_file;
                    return true;
               } else {
                    $msg .= " move_upload_file failed.  ";
               }
          }
          throw new \Exception("Sorry, there was an error uploading your file.  $msg , The system is now stoping");
     }

     private function hasFileError($fileErrorCode) {
          switch ($fileErrorCode) {
               case (UPLOAD_ERR_OK):
                    //Value: 0;
                    //There is no error, the file uploaded with success.
                    break;
               case (UPLOAD_ERR_INI_SIZE):

               //     Value: 1;
               //     The uploaded file exceeds the upload_max_filesize directive in php.ini.
               case (UPLOAD_ERR_FORM_SIZE):
                    //Value: 2;
                    //The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form.
               case (     UPLOAD_ERR_PARTIAL):

                    //Value: 3;
                    //The uploaded file was only partially uploaded.
               case (     UPLOAD_ERR_NO_FILE):

                    //Value: 4;
                    //No file was uploaded.
               case (     UPLOAD_ERR_NO_TMP_DIR):

                    //Value: 6;
                    //Missing a temporary folder. Introduced in PHP 5.0.3.
               case (UPLOAD_ERR_CANT_WRITE):

               //Value: 7;
               //     Failed to write file to disk. Introduced in PHP 5.1.0.
               case (UPLOAD_ERR_EXTENSION):

               //Value: 8;
               //     A PHP extension stopped the file upload. PHP does not provide a way to ascertain which extension caused the file upload to stop;
               //     examining the list of loaded extensions with phpinfo() may help. Introduced in PHP 5.2.0.
               default:
                    //some other code
          }
          return $fileErrorCode;
     }

     function deleteFiles() {
          foreach ($this->fileList as $fileName) {
               if (file_exists($fileName)) {
                    unlink($fileName);
               }
          }
          $this->fileList = [];
     }

     function getUploadCount() {
          return count($this->fileList);
     }

     function getUploadedFileNames() {
          return $this->fileList;
     }

     function getSizeLimit() {
          return $this->sizeLimit;
     }

     /**
      * 
      * @param type $sizeLimit set to 0.5 meg increased to server maximum
      * @return \Consulting247\Widgets\FileUploader\FileUploaderManager
      */
     function setSizeLimit($sizeLimit) {
          $this->sizeLimit = $sizeLimit;
          return $this;
     }

}

/*
  //additional code that might be useful
  // Check if image file is a actual image or fake image
  if (isset($_POST["submit"])) {
  $check = getimagesize($_FILES[$fileWidgetVariableName]["tmp_name"]);
  if ($check !== false) {
  echo "File is an image - " . $check["mime"] . ".";
  $uploadOk = 1;
  } else {
  echo "File is not an image.";
  $uploadOk = 0;
  }
  }


  // Allow certain file formats
  if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif") {
  echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
  $uploadOk = 0;
  }
 */

