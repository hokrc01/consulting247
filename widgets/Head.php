<?php
/**
 * Description of newPHPClass
 *
 * @author ken
 */
namespace Consulting247\Widgets;
class Head {
	//put your code here
	private $title;
	private $scripts;
	private $media = array();
	private $library = array();
	private $includeFile=array();
	private $oemCSS=array();
	
	public function getOEMCSS() {
		return $this->oemCSS;
	}

	public function addOEMCSS($oemCSS) {
		$this->oemCSS[] = $oemCSS;
                return $this;
	}
	
	public function getIncludeFile() {
		return $this->includeFile;
	}

	public function addIncludeFile($includeFile) {
		$this->includeFile[] = $includeFile;
                return $this;
	}

	public function addOEMScript($library){
		$this->library[]=$library;
                return $this;
	}
	
	public function getOEMScript(){
		return $this->library;
	}

	public function getTitle() {
		return $this->title;
	}

	public function setTitle($title) {
		$this->title = "$title";
                return $this;
	}

	public function getScripts() {
		return $this->scripts;
	}

	public function addScript($filename) {
        	$this->scripts[] = strtolower($filename);
                return $this;
		

	}

	public function getMedia() {
		return $this->media;
	}

	public function setMedia(array $media) {
		$this->media[] = $media;
                return $this;
	}
	
	public function addMedia($typeOfMedia,$cssName){
		$this->media[$typeOfMedia] = $cssName;
	}




    function render(){ ?>
	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title><?=$this->title?></title>
		<meta name="description" content="" />
		<meta name="keywords" content="" />
		<meta http-equiv="Content-Type" content="text/html;charset=UTF-8" />
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
		<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/themes/smoothness/jquery-ui.css" />
                <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.2/jquery-ui.min.js"></script>
	
			<?php
			
			if (count($this->getOEMCSS())){
				foreach ($this->getOEMCSS() AS $key=>$library){
					if (file_exists($library)){	
						echo "<link href=\"$library\" rel=\"stylesheet\" type=\"text/css\" />";

					}
					else{
						throw new \Exception("$library css file does not exist or no access");
					}				
				}
			}
			
			if (count($this->getOEMScript())){
				foreach ($this->getOEMScript() AS $key=>$library){
					if (file_exists($library)){	
						echo "<script type=\"text/javascript\" src=\"$library\"></script>";
					}
					else{
						throw new \Exception("$library javascript does not exist or no access");
					}				
				}
			}
			
			if (count($this->getIncludeFile())){
				foreach ($this->getIncludeFile() AS $key=>$includeFile){
					if (file_exists($includeFile)){	
						include_once("$includeFile");
					}
					else{
						throw new \Exception("$includeFile include file was not found or no access");
					}				
				}
			}
			
			?>
			<!-- //my libraries -->
			<link href="css/default.css" rel="stylesheet" type="text/css" />
			<link href="css/menubar.css" rel="stylesheet" type="text/css" />
			
			<?php
			if ($this->scripts)foreach ($this->scripts AS $key=>$baseFileName){
				$css = "css/$baseFileName.css";
				if (file_exists($css)){
					echo "<link href=\"$css\" rel=\"stylesheet\" type=\"text/css\" />";
				}
			
				$js = "scripts/$baseFileName.js";
				if (file_exists($js)){
					echo '<script type="text/javascript" src="'.$js.'"></script>';
				}			
			
			}//end loop
		
			if ($this->media)foreach ($this->media AS $key=>$baseFileName){
				$fn = "css/$baseFileName.css";
				if (file_exists($fn)){
					echo "<link href=\"$fn\" rel=\"stylesheet\" type=\"text/css\" media=\"$key\" />";
				}
			
			}//end loop
	echo "</head>";
    }//end render 
	
}//end class
?>