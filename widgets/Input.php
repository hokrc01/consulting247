<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Consulting247\Widgets;

/**
 * Description of Input
 *
 * @author ken
 */
class Input extends HTMLWidget {

     private $type = "text";
     private $divClass = null;
     private $placeholder = null;


     function render() {                   
          $widget = ($this->li)?'<li class="input-wrapper ' . $this->liClass . " $this->widgetClass-li " . '">':null;
          $widget .= (isset($this->label)) ? "<label class=\"input-label  $this->widgetClass-label\">$this->label</label>" : '';
          $this->id = (is_null($this->id)) ? $this->name : $this->id;

          $widget .= "<div id=\"$this->id-div\" class=\"input input-box $this->divClass $this->widgetClass-div\">" .
               "<input type=\"{$this->type}\" id=\"$this->id\" "
               . " class=\"$this->class $this->widgetClass-input\" name=\"$this->name\" value=\"$this->value\" "
               . " $this->placeholder $this->otherProperties />";

          if ((strlen(trim($this->errorMsg)) > 0)||($this->errorClass)) {
               $widget .= '<div class="input-error error-1 ' . $this->class . 'Error '
                    . $this->widgetClass . '-error '.$this->errorClass.'">' .
                    $this->errorMsg .
                    '</div>';
          }
          $widget .= '</div>';
          $widget .= ($this->li)?'</li>':null;
          echo $widget;
     }

//end render
     //setters

     function setType($type) {
          $this->type = $type;
          return $this;
     }

     function setDivClass($divClass) {
          $this->divClass = $divClass;
          return $this;
     }

     function setPlaceholder($placeholder) {
          $this->placeholder = "placeholder=\"$placeholder\"";
          return $this;
     }
    

}
