<?php

namespace Consulting247\Widgets;

class DataTable {

    private $data;
    private $header;
    private $displayColumns;
    private $sort;
    private $checkbox = false;
    private $checkboxClass = null;
    private $cbTitle="&nbsp;";
    private $id = "scrollable-table";
    private $class = '';
    private $checkAll = false;
    private $zerosAsBlank=true;
    
    function getId() {
        return $this->id;
    }

    function setId($id) {
        $this->id = $id;
        return $this;
    }

        function setClass($class) {
        $this->class = $class;
        return $this;
    }
    
    public function setZeroAsBlank($zeroAsBlank){
         $this->zeroAsBlank = $zeroAsBlank;
         return $this;
    }
    public function getCheckboxClass() {
        return $this->checkboxClass;
    }

    public function setCheckboxClass($var) {
        $this->checkboxClass = $var;
        return $this;
    }

    public function getCheckbox() {
        return $this->checkbox;
    }

    public function setCheckbox($checkbox, $checkAll = false,$cbTitle="&nbsp;") {
        $this->checkbox = $checkbox;
        $this->checkAll = $checkAll;
        $this->cbTitle = $cbTitle;
        return $this;
    }

    public function getDisplayColumns() {
        return $this->displayColumns;
    }

    public function setDisplayColumns(array $displayColumns) {
        $this->displayColumns = $displayColumns;
        return $this;
    }

    public function setData($data) {
        $this->data = $data;
        return $this;
    }

    public function setHeader(array $header) {
        $this->header = $header;
        return $this;
    }

    public function getSort() {
        return $this->sort;
    }

    public function setSort($sort) {
        $this->sort = $sort;
    }

    function render() {
        if (!is_array($this->data)) {
            echo "<div class=\"center\">No information entered</div>";
            return;
        }

        if ($this->data) {
            echo '<div id="' .
            $this->id
            . '" class="table  '.$this->class.'-table">';
            echo '<div class="scrollable row-0 table-header">';
            if (is_array($this->header)) {
                if ($this->getCheckbox()) {

                    echo "<span id=\"header-col-cb\" "
                    . "class=\"cols col-cb column-header\"  >$this->cbTitle</span>";
                }
                foreach ($this->header AS $key => $header) {
                    if ($this->getSort()) {
                        echo "<span id=\"cols header-col-$key\" class=\"col-$key column-header\"  >$header</span>";
                    } else {
                        echo "<span class=\"cols col-$key \"  >$header</span>";
                    }
                }
            } else {
                echo "&nbsp;";
            }

            echo "</div>";

            echo "<div id=\"data\" class=\"scrollable table-height\">";
            $cnt = 0;

            //initialize variable to set default as checked
            $checked = ($this->checkAll) ? "checked=checked" : null;
            foreach ($this->data AS $row) {
                //cluge
                if (!key_exists('id', $row)) {
                    $row['id'] = $cnt;
                }
                //end cluge
                echo "<div id=\"row-{$row['id']}\" class=\" data-row row-" . ($cnt % 2) . "\">";
                if ($this->getCheckbox()) {
                    if ($this->getCheckboxClass()) {
                        $checkboxId = "class=\"{$this->getCheckboxClass()}\"";
                    } else {
                        $checkboxId = null;
                    }

                    echo "<span class=\"cols col-cb\">"
                    . "<input type=\"checkbox\" $checkboxId name=\"id[{$row['id']}]\" value=\"{$row['id']}\" $checked />"
                    . "</span>";
                }

                if ($this->displayColumns) {

                    $tColumnArray = $this->getDisplayColumns();
                    foreach ($tColumnArray AS $index => $field) {

                        if ($row[$field]){
                             $value1 = $row[$field];
                        }
                        else{
                             $value1 = ($this->zerosAsBlank)?'&nbsp;':$row[$field];
                             
                        }
                        echo "<span class=\"cols col-$index\">$row[$field]</span>";

                    }
                } else {
                    foreach ($row AS $index => $field) {
                        echo "<span class=\"cols col-$index\">$field</span>";
                    }
                }

                echo "</div>";
                $cnt++;
            }//end
            echo "</div>";
            echo "</div>";
        } else {
            echo "<div class=\"center\">No information entered</div>";
        }




        /*
          <p class="center">
          <button id="newMember" name="action" value="NewMember">New Member</button>
          <button id="editMember" name="action" value="editMember">Edit Member</button>
          <button id="cancel" name="action" value="cancel">Cancel</button>
          </p>
          </form>
          </div>
          <?php
         * 
         */
    }

//end render
}

?>
