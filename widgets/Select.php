<?php
namespace Consulting247\Widgets;
/**
 * @abstract creates a select tag.  If you need a Combo box use
 * this tag and user jquery and jquery.ui and $(#id).comboBox();
 */
class Select extends HTMLWidget {
    private $options=[];
    private $selectedOption;
    private $wrapper=true;

    private $optionsValueIndex = "id";
    private $optionsTextIndex = "name";
    
    private $isComboBox=false;
    private $javaScriptStub = true;   
    
    function render(){
        if(is_null($this->name)){$this->name="default_select_tag_name";}
        if (is_null($this->id)) {$this->id=$this->name;}
        $widget = '';

        if ($this->li) {$widget .= '<li id="'
            ."$this->id-li"
            .'" class="input-wrapper '.$this->widgetClass.'-li">';}
         
        if (isset($this->label)){$widget .= "<label id=\"$this->id-label\" class=\"$this->widgetClass-label\">$this->label</label>";}
        
	if ($this->wrapper){
            $widget .= "<div id=\"$this->id-wrapper\" class=\"$this->id-wrapper input-div input-select-list $this->widgetClass-select\">";
        }

            
        if (!is_array($this->options)){
            $widget .= "<span id=\"$this->id\" class=\"select-div $this->class $this->id\">"
                . "No List Found</span>";
        }
        elseif (is_array(current($this->options))) {
            $widget .= $this->makeSelectFromDatabase();
        }
        else{
            $widget .= "<select id=\"$this->id\" class=\"select-div $this->class \" name=\"$this->name\">";
            foreach ($this->options AS $optionsValueIndex=>$text){
                if (is_numeric($optionsValueIndex) && is_numeric($this->selectedOption)){
                    $selected = ((float)$optionsValueIndex === (float)$this->selectedOption)?'selected="selected"':null;
                }else{
                    $selected = ($optionsValueIndex === $this->selectedOption)?'selected="selected"':null;
                }
                $widget .= "<option value=\"$optionsValueIndex\" $selected>$text</option>";
            }
            if ($this->isComboBox){
                $selected = ("other" == $this->selectedOption)?'selected="selected"':null;
                $widget .= "<option value=\"other\" $selected>Other, please specify:</option>";
            
            }
            $widget .= '</select>';
        }
        
        
        if ($this->isComboBox){
            $hide=("other" != $this->selectedOption)?'hide':null;
            $widget .= "<input type=\"text\" id=\"$this->id-input\" class=\"$hide $this->id-input $this->class-input\" "
                . "name=\"{$this->name}_input\" >";
        }
        if($this->wrapper){
            $widget .= '</div>';
        }
        
        if (strlen(trim($this->errorMsg))>0){
            $widget .= '<div class="input-error error-1 '.$this->class.'Error">'.
                        $this->errorMsg.
                        '</div>';
        }
        if ($this->isComboBox){
            $widget .= $this->renderJavaScriptStub();
        }
        if ($this->li){$widget .= '</li>';}

        echo $widget;
    }//end render
    
    private function makeSelectFromDatabase(){
        if (!key_exists($this->optionsValueIndex, current($this->options))){
            throw new \Exception('Programmer you have set the option value to '
                . "'$this->optionsValueIndex'<br> "
                . 'Use method SetOptionsValueIndex($value)'
                . ' Where $value is an index of the multiple array set by $SetOptions($list) '
                . 'that you want to set for the value option in the select statment');
                    
        }
        
        if (!key_exists($this->optionsTextIndex, current($this->options))){
            throw new \Exception('Programmer you have set the option value to '
                . "'$this->optionsTextIndex' "
                .' Use method SetOptionsTextIndex($value)'
                .' Where $value is an index of the multiple array set by $SetOptions($list)'
                .' that you want to display as the option text in the select');
        }
        
        $widget = null;    
        $widget .= "<select id=\"$this->id\" class=\"select-div $this->class \" name=\"$this->name\">";
        foreach ($this->options AS $index=>$option){
                $selected = ($option[$this->optionsValueIndex] == $this->selectedOption)?'selected="selected"':null;
                $widget .= "<option value=\"{$option[$this->optionsValueIndex]}\" $selected>{$option[$this->optionsTextIndex]}</option>";
        }

        if ($this->isComboBox){
                $selected = ("other" == $this->selectedOption)?'selected="selected"':null;
                $widget .= "<option value=\"other\" $selected>Other, please specify:</option>";
            
            }
        $widget .= '</select>';

        return $widget;
    }


    private function renderJavaScriptStub(){
        if (!$this->javaScriptStub){return null;}
        return '<script>
             //code located in Consulting247\Widgets\Select
            //use php method ->setJavaScriptStub(false) to not print stub in your code
            $(document).ready(function(){   
            $("#'.$this->id.'").on("change",function(){
                if ($(this).val()==="other"){
                    $("#'.$this->id.'-input").val("");
                    $("#'.$this->id.'-input").addClass("block").removeClass("hide");
                         
                }
                else{
                    $("#'.$this->id.'-input").addClass("hide").removeClass("block"); 
                }
         
            });
            //try avoid duplicates 
            $("#'.$this->id.'-input").on("blur",function(){        
                var newText = $(this).val().toLowerCase();
                if($("#'.$this->id.'").val()==="other"){
                $("#'.$this->id.' > option").each(function(optionValue,item){
                    if (newText === $(item).text().toLowerCase()){
                        $("#'.$this->id.'").val($(item).val()).change();
                    
                }
            });
        }
    });
            });  

            </script>';
    }
  



    //setters
     /**
      * @uses put element on top if not numeric
      * @param type $index
      * @param type $value
      */
    function pushOption($index, $value){
        
        if (!is_array(current($this->options)) && is_numeric($index) && (key($this->options)<=$index)){
            throw new \Exception('This method may not be used to replace a value in the option list.  Index must be less than "'
                . key($this->options)
                .'"');
        }
        
        $this->options = array_merge([$index=>$value],$this->options);
        return $this;
    }
    /**
     * @uses element can also replace an element in the list
     * @param type $index
     * @param type $value
     * @return \Consulting247\Widgets\Select
     */
    function addOption($index,$value){
        $this->options[$index]=$value;
        return $this;
            
        
       
    }
    
    function setOptions(array $optionsArray){
        if (empty($this->options)){
            $this->options = $optionsArray;
        }
        else{
            //merge arrays
            $merge = $this->options + $optionsArray;
            $this->options = $merge;
        }
        return $this;
    }

    //overide parent
    function setValue($value){
        $this->selectedOption= $value;
        return $this;
    }
    function getSelectedOption() {
        return $this->selectedOption;
    }

    function setSelectedOption($selectedOption) {
        $this->selectedOption = $selectedOption;
        return $this;
    }

    function getWrapper() {
        return $this->wrapper;
    }

    function setWrapper($wrapper) {
        $this->wrapper = $wrapper;
         return $this;
    }


    function setOptionsValueIndex($var){
        $this->optionsValueIndex=$var;
        return $this;
    }
    function setOptionsTextIndex($var){
        $this->optionsTextIndex = $var;
        return $this;
    }

    function setIsComboBox($trueorfalse){
        $this->isComboBox=(bool)$trueorfalse;
        return $this;
    }
    
    function setJavaScriptStub($trueorfalse){
        $this->javaScriptStub=(bool)$trueorfalse;
        return $this;
    }
}