<?php
namespace Consulting247\Widgets;

abstract class BodyAbstract {
	private $authority;
	private $subheader;
	private $showMenu=false;

        abstract function renderBanner();
        //abstract function getMenu();
        
        function setShowMenu($option){$this->showMenu = $option;}
	function setSubheader($option){$this->subheader = $option;}
	
	function __construct($authority=null) {
		$this->authority=$authority;
		$this->setShowMenu(true);
		$this->setSubheader(false);
		
	}
	
	
    function renderHeader($subhead=null){
    ?>
    <body>
    	<div id="page">
            <?php $this->renderBanner($subhead); ?>
           

    <?php
    }//end renderHeader
	
    function renderFooter(){
        $today = new \DateTime();
	?>	
	<div id="footer">
            <span class="float-left">
		&copy; Copyright 2010 - <?=$today->format('Y')?>. 
		HOKRC,LLC
            </span>
            <span class="float-right">
                <?php if ($this->showMenu)
                    $this->renderMenu();
							?>
            </span>
	</div>
	<?php 
	echo "</div><!--end page --></body></html>";
	}
	
	private function renderMenu(){
		//$security = new Security();
		switch ("temp"){//$security->getAuthority()) {
			case ("admin"):?>
				<ul>
					<li class="menu-first-item"><a href="<?=Page::getLocation("admin")?>">Home</a></li>
					<li class="menu-first-item"><a href="<?=Page::getLocation("adminSchools")?>">Schools</a></li>
					<li class="menu-first-item"><a href="<?=Page::getLocation("adminMembers")?>">Membership</a></li>
					<li class="menu-first-item"><a href="<?=Page::getLocation("invoiceProcessing")?>">Invoices</a></li>
					<li class="menu-first-item"><a href="<?=Page::getLocation("printing")?>">Printing</a></li>
					<li class="menu-first-item"><a href="<?=Page::getLocation("communicator")?>">Communications</a></li>
					<li class="menu-first-item"><a href="<?=Page::getLocation("manageFees")?>">Fees</a></li>
					<li class="menu-first-item"><a href="<?=Page::getLocation("manageBelt")?>">Belts</a></li>
					<li class="menu-first-item"><a href="<?=Page::getLocation("index")?>">Logout</a></li>

				</ul>	
			<?php break;
		case ("registrar"):?>
				<ul>
					<li class="menu-first-item"><a href="<?=Page::getLocation("registrar")?>">Home</a></li> 
					<li class="menu-first-item"><a href="<?=Page::getLocation("adminMembers")?>">Membership</a></li>
					<li class="menu-first-item"><a href="<?=Page::getLocation("index")?>">Logout</a></li>

				</ul>	
			<?php break;
			case ("adminold"):?>
			<ul>
				<li class="menu-first-item"><a href="<?=Page::getLocation("admin")?>">Home</a></li>
				<li><a href="">Process</a>
					<ul class="inner-menu">
						<li><a href="<?=Page::getLocation("admin",array("option"=>"School"))?>">School Applications</a></li>
						<li><a href="<?=Page::getLocation("managTable",array("option"=>"country"))?>">Under Development</a></li>
					</ul>
				</li>
				<li>
					<a href="">Base Tables</a>
					<ul class="inner-menu">
						<li><a href="<?=Page::getLocation("manageTable",array("tablename"=>"belt"))?>">Belt</a></li>
						<li><a href="<?=Page::getLocation("manageTable",array("tablename"=>"country"))?>">Country</a></li>
					</ul>
					
				</li>
				
			<!--	<li><a href="#">Recent articles</a></li>
				<li><a href="#">Email</a></li>
				<li><a href="#">Resources</a></li>
				<li><a href="#">Links</a></li>
				<li><a href="#">Contact</a></li>
				-->
			</ul><?php 


				break;
			
			case ("user"):?>
			<ul>
			<li style="border-left: medium none;"><a href=<a href="<?=Location::manage()?>">Home</a></li>
			<!--			<li><a href="#">About Us</a></li> -->
						<li><a href="<?=Location::reporting()?>">Reporting</a></li>
						<li><a href="<?=Location::questionnaire()?>">Questionnaire</a></li>					
						<li><a href="<?=Location::registerII()?>">Personal Planning Resource</a></li>
						<!--<li><a href="#">Email</a></li>
			<!--		<li><a href="#">Resources</a></li>
					<li><a href="#">Links</a></li>
					<li><a href="#">Contact</a></li>  -->
			</ul><?php 
				break;
			
			case ("school"):
				?>
				<ul>
				<li class="menu-first-item"><a href="<?=Page::getLocation("school")?>">Membership</a></li>
				<li class="menu-first-item"><a href="<?=Page::getLocation("ranking")?>">Ranking</a></li>
				<li class="menu-first-item"><a href="<?=Page::getLocation("accounting")?>">Accounting</a></li>
				<li class="menu-first-item"><a href="<?=Page::getLocation("index")?>">Logout</a></li>


			</ul><?php 


				break;
			
			
			default:
				//no menue
				break;
		}
		

	}
	

//*/	
}

?>
