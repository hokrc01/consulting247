<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace Consulting247\Widgets;

/**
 * Description of Input
 *
 * @author ken
 */
class Hidden extends HTMLWidget {
    
    function render(){
        $widget = '';
		
	if (is_null($this->id)) $this->id=$this->name;
        
	$widget .= "<input type=\"hidden\" id=\"$this->id\" class=\"$this->class\" name=\"$this->name\" value=\"$this->value\" />";

        echo $widget;
    }//end render
        //setters
}
