<?php

namespace Consulting247;

class SiteMap {

     /**
      * 
      * @param type $page_id
      * @param type $parameters get line parameters
      */
     function header($page_id, $parameters = null) {
          $parameters = $this->formatParameters($parameters);
          if (is_null($parameters)) {
               header("location:" . $this->getScript($page_id));
          } else {
               header("location:" . $this->getScript($page_id) . "?$parameters");
          }
          exit;
     }

     function render($page_id, $parameters = null) {
          $parameters = $this->formatParameters($parameters);
          if (is_null($parameters)) {
               return $this->getScript($page_id);
          } else {
               $uri = $this->getScript($page_id);
               if (strpos($uri, "?") === false) {
                    return $uri . "?$parameters";
               } else {
                    return $uri . $parameters;
               }
          }
     }

     function getNextPage($page_id) {
          return $this->getScript($page_id, 2);
     }

     function getIncludeScript($page_id) {
          return $this->getScript($page_id, 1);
     }

     private function getScript($page_id, $returnFormat = 0, $type = null) {
          if (is_null($page_id)) {
               $site[0]['name'] = 'index.php';
               $site[0]['protocal']="http";
          } else {
               $db = new Database();
               $site = $db->select("SELECT * FROM site_map WHERE id = :id ", array("id" => $page_id));              
          }
          //take the request sceam and roll it forward
          //$site[0]['protocal'] = filter_input(INPUT_SERVER,  'REQUEST_SCHEME',FILTER_SANITIZE_STRING);
       

          //godaddy workarround
          if (($_SERVER['SERVER_NAME'] === 'localhost') ||
               ($_SERVER['SERVER_NAME'] === '45.58.35.168')
          ) {
               $t = explode("/", dirname($_SERVER['PHP_SELF']));
               $directory = "/{$t[1]}/";
          } else {
               $directory = "/";
          }



          if (empty($site)) {

               return "http://" .
                    $_SERVER['HTTP_HOST'] .
                    $directory .
                    "under_construction.php";
          }

          if (((int) $site[0]['is_parameters_only']) === 1) {
               return $this->getCurrentURI() . "?" . $site[0]['name'];
          }

          switch ($returnFormat) {
               case(1):
                    return $site[0]['name'];
                    break;

               case(2):
                    return $site[0]['protocal'] . '://' .
                         $_SERVER['HTTP_HOST'] .
                         $directory . '?pageId=' .
                         $site[0]['name'];
                    break;
               default:
                    return $site[0]['protocal'] . '://' .
                         $_SERVER['HTTP_HOST'] .
                         $directory .
                         $site[0]['name'];
          }
     }

     function getCurrentURI() {
          $requestScheme = filter_input(INPUT_SERVER, "REQUEST_SCHEME", FILTER_SANITIZE_URL);
          $host = filter_input(INPUT_SERVER, "HTTP_HOST", FILTER_SANITIZE_URL);
          $tUri = filter_input(INPUT_SERVER, "REQUEST_URI", FILTER_SANITIZE_URL);
          $t = explode("?", $tUri);
          $uri = $t[0];
          $params = null;
          if (count($t) > 1) {
               unset($t[0]);
               foreach ($t AS $questionMark) {
                    $params .= $questionMark;
               }
          }
          $newUri = "$requestScheme://$host" . $uri;
          //$params is currently un used, might be needed
          return $newUri;
     }

     /**
      * @uses element This function is used to convert and array to a string for a GET style url call
      * @param type $params 
      * @return type
      */
     private function formatParameters($params) {
          if (is_array($params)) {
               $paramString = null;
               foreach ($params AS $variableName => $value) {
                    $paramString .= "$variableName=$value&";
               }
               return (substr($paramString, 0, -1));
          }

          return($params);
     }

     static function action($view = "index", $request_scheme = "https://") {
          $host = filter_input(INPUT_SERVER, "HTTP_HOST", FILTER_SANITIZE_STRING);
          $phpSelf = filter_input(INPUT_SERVER, "PHP_SELF", FILTER_SANITIZE_STRING);

          return $request_scheme . $host . dirname($phpSelf)."/" . $view;
     }

     static function redirect($view = "index", $request_scheme = "https://") {
          header("location:".self::action($view, $request_scheme));
          exit;
     }

}
