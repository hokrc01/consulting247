<?php
namespace Consulting247;

/**
 * Generic Response class Description of Response
 *
 * @author ken
 */
class Response{
    private $success;
    private $code;
    private $data;

    /**
     * 
     * @param type $success should be value such as true or false
     * @param type $data any return data needed 
     */
    function __construct($success, $data=null) {
        $this->success = $success;
        $this->data = $data;
    }
    
    //  Getters
    function isSuccessful() {
        return $this->success;
    }

    function getData() {
        return $this->data;
    }
    function setData($data){
        $this->data = $data;
        return $this;
    }

    function getCode() {
        return $this->code;
    }

    function setCode($code) {
        $this->code = $code;
        return $this;
    }


}