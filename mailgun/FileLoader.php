<?php

namespace Consulting247\mailgun;

/**
 * Description of FileLoader
 *
 * @author ken
 */
class FileLoader {
/**
 * 
 * @param type $filename
 * @param type $dataStructure anytype of data structure to be read by file described
 * @return type
 */
    private $text;//
    
    function loadExternalText($filename,$dataStructure=null){
			
        if (file_exists($filename)){
            ob_start();
            include $filename;
            $textCopy = ob_get_clean();
            $this->text=$textCopy;
            return $textCopy;
	}
		
	return null;
    }

    function getText(){
        return $this->text;
    }
}
